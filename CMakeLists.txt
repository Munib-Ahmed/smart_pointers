cmake_minimum_required(VERSION 3.1.0)
project(smart_pointers VERSION 1.0.0)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})


option(DEBUG "Use this option to build in debug mode" OFF)

if(DEBUG)
    set(CMAKE_BUILD_TYPE Debug)
    message("DEBUG mode")
else()
    set(CMAKE_BUILD_TYPE Release)
    message("RELEASE mode")
endif()
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")


add_subdirectory(src)
add_subdirectory(app)

