#ifndef DATABASE_H
#define DATABASE_H

#include <list>
#include <memory>
#include "student.h"

namespace emumba
{
    namespace training
    {
        class database
        {
        private:
            std::map<std::string, std::shared_ptr<student>> student_record;

        public:
            database();
            std::shared_ptr<student> get_student_reference_shared(const std::string &student_name) const;
            std::unique_ptr<student> get_student_reference_unique(const std::string &student_name) const;
            void print_student_data(const std::shared_ptr<student> &student_pointer) const;
            void print_student_data(const std::unique_ptr<student> &student_pointer) const;
            void add_new_student(const student &, const std::string &);
            void print_all_students_name() const;
        };
    }
}
#endif
