#ifndef STUDENT_H
#define STUDENT_H

#include <map>
#include <string>
#include <iterator>

namespace emumba
{
	namespace training
	{
		class student
		{
		private:
			struct student_record
			{
				std::string roll_no;
				int age;
				float cgpa;
			};
			student_record st_object;

			std::map<std::string /*subject name*/, int /*marks*/> result;

		public:
			student(const std::string &course, const int &marks, const float &gpa, const std::string &roll_no, const int &age);
			int get_subject_marks(const std::string &subject) const;
			void set_subject_marks(const std::string &subject, const int &marks);
			void print_all_marks() const;

			void set_cgpa(const float &gpa);
			void set_age(const int &age);
			void set_roll_no(const std::string &roll_no);
			const float &get_cgpa() const;
			const int &get_age() const;
			const std::string &get_roll_no() const;
		};
	}
}

#endif