# smart_pointers

## Getting started

this is a simple project, in which we will be using our student class from the previous task and adding some new features in it by the help of the some new classes.

- clone this project into your system, using the following command;

```bash
git clone https://gitlab.com/Munib-Ahmed/smart_pointers.git
```

- Enter the cloned Directory, with:

```bash
cd smart_pointers
```

- Now, we need to create a directory typically named as "build"

```bash
mkdir build
```

- navigate to build directory,

```bash
cd build
```

## How to Run:

- simply navigate to the build directory and enter the following commands in terminal:

```bash
cmake ..
cmake --build .
./smart_pointers
```
