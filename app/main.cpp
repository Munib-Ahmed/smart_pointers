#include <iostream>
#include "student.h"
#include "database.h"
using namespace std;
using namespace emumba::training;

int main()
{
    database data_student;
    student student_data("maths", 10, 6.6, "i15-006", 6);
    data_student.add_new_student(student_data, "ali 6");
    data_student.add_new_student(student_data, "ali 7");
    data_student.print_all_students_name();

    const std::shared_ptr<student> &student_data_pointer = data_student.get_student_reference_shared("ali 7");
    if (student_data_pointer != nullptr)
    {
        cout << "CGPA = " << student_data_pointer->get_cgpa() << endl;
        cout << "use.count = " << student_data_pointer.use_count() << endl;
        student_data_pointer->set_subject_marks("bio", 68);

        data_student.print_student_data(student_data_pointer);
    }

    std::unique_ptr<student> student_data_pointer_1 = data_student.get_student_reference_unique("ali 6");
    if (student_data_pointer_1 != nullptr)
    {
        cout << "CGPA = " << student_data_pointer_1->get_cgpa() << endl;
        student_data_pointer_1->set_subject_marks("physics", 50);
        data_student.print_student_data(student_data_pointer_1);
    }

    std::unique_ptr<student> student_data_pointer_2 = data_student.get_student_reference_unique("ali 5");
    if (student_data_pointer_2 != nullptr)
    {
        cout << "CGPA = " << student_data_pointer_2->get_cgpa() << endl;
        data_student.print_student_data(student_data_pointer_2);
    }
    return 0;
}