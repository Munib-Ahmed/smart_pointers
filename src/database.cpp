#include <iostream>
using namespace std;
#include "database.h"
using namespace emumba::training;

database::database()
{
	cout << "databse constructor" << endl;

	student_record["ali 1"] = std::make_shared<student>(student("maths", 1, 1.1, "i15-001", 11));
	student_record["ali 2"] = std::make_shared<student>(student("maths", 2, 2.2, "i15-002", 12));
	student_record["ali 3"] = std::make_shared<student>(student("maths", 3, 3.3, "i15-003", 13));
	student_record["ali 4"] = std::make_shared<student>(student("maths", 4, 4.4, "i15-004", 14));
	student_record["ali 5"] = std::make_shared<student>(student("maths", 5, 5.5, "i15-005", 15));
}

std::shared_ptr<student> database::get_student_reference_shared(const std::string &student_name) const
{
	std::map<std::string, std::shared_ptr<student>>::const_iterator itr = student_record.find(student_name);
	if (itr != student_record.end())
	{
		return itr->second;
	}
	cout << "\t \t student not found" << endl;
	return nullptr;
}

std::unique_ptr<student> database::get_student_reference_unique(const std::string &student_name) const
{
	std::map<std::string, std::shared_ptr<student>>::const_iterator itr = student_record.find(student_name);
	if (itr != student_record.end())
	{
		std::unique_ptr<student> ptr_return = std::make_unique<student>(*itr->second);
		return ptr_return;
	}
	cout << "\t \t student not found" << endl;
	return nullptr;
}

void database::print_student_data(const std::shared_ptr<student> &student_pointer) const
{
	if (student_pointer != nullptr)
	{
		cout << endl;
		cout << "	-----	printing student data 	-----" << endl;
		cout << "Student Roll no. = " << student_pointer->get_roll_no() << endl;
		cout << "Student Age = " << student_pointer->get_age() << endl;
		cout << "Student CGPA = " << student_pointer->get_cgpa() << endl;
		student_pointer->print_all_marks();
		cout << endl;
	}
}

void database::print_student_data(const std::unique_ptr<student> &student_pointer) const
{
	if (student_pointer != nullptr)
	{
		cout << endl;
		cout << "	-----	printing student data 	-----" << endl;
		cout << "Student Roll no. = " << student_pointer->get_roll_no() << endl;
		cout << "Student Age = " << student_pointer->get_age() << endl;
		cout << "Student CGPA = " << student_pointer->get_cgpa() << endl;
		student_pointer->print_all_marks();
		cout << endl;
	}
}

void database::add_new_student(const student &new_student, const std::string &name)
{
	student_record[name] = std::make_shared<student>(new_student);
}

void database::print_all_students_name() const
{
	map<std::string, std::shared_ptr<student>>::const_iterator it = student_record.begin();
	for (; it != student_record.end(); ++it)
	{
		cout << it->first << endl;
	}
}