#include <iostream>
#include "student.h"
using namespace std;
using namespace emumba::training;

student::student(const string &course, const int &marks, const float &gpa, const string &roll_no, const int &age)
{
	result[course] = marks;
	st_object.age = age;
	st_object.cgpa = gpa;
	st_object.roll_no = roll_no;
}

int student::get_subject_marks(const string &subject) const
{
	std::map<std::string, int>::const_iterator itr = result.find(subject);
	if (itr != result.end())
	{
		return itr->second;
	}
	cout << "\t \t course not found" << endl;
	return -1;
}

void student::set_subject_marks(const string &subject, const int &marks)
{
	result[subject] = marks;
}

void student::print_all_marks() const
{
	cout << '\t' << "Subjects " << '\t' << '\t' << " Marks" << endl;
	for (std::map<std::string, int>::const_iterator itr = result.begin(); itr != result.end(); ++itr)
	{
		cout << '\t' << itr->first << '\t' << '\t' << itr->second << endl;
	}
}

void student::set_cgpa(const float &gpa)
{
	st_object.cgpa = gpa;
}

void student::set_age(const int &age)
{
	st_object.age = age;
}

void student::set_roll_no(const string &roll_no)
{
	st_object.roll_no = roll_no;
}

const float &student::get_cgpa() const
{
	return st_object.cgpa;
}

const int &student::get_age() const
{
	return st_object.age;
}

const string &student::get_roll_no() const
{
	return st_object.roll_no;
}
